import { RegisterComponent } from './components/register.component';
import { CategoriaComponent } from './components/categoria.component';
import { ServiceComponent } from './components/service.component';
import { PortafolioAddComponent } from './components/portafolio-add.component';
import { PerfileditComponent } from './components/perfil-edit.component';
import { PostListComponent } from './components/post-list.component';
import { SobremiComponent } from './components/sobremi.component';
import { AppComponent } from './app.component';
import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { HomeComponent } from './components/home.component';
import { AdminComponent } from './components/admin.component';
import { PostDetailsComponent } from './components/post-details.component';
import { PerfilAddComponent } from './components/perfil-add.component';
import { EducacionAddComponent } from './components/educacion-add.component';
import { EducacioneditComponent } from './components/educacion-edit.component';
import { PortafolioComponent } from './components/portafolio.component';
import { PortafolioeditComponent } from './components/portafolio-edit.component';
import { ContactoComponent } from './components/contacto.component';
import { PostAddComponent } from './components/post-add.component';
import { PosteditComponent } from './components/post-edit.component';
import { UserListComponent } from './components/user-list.component';
import { User } from './models/user';
import { UserEditComponent } from './components/user-edit.component';
import { PostFilterComponent } from './components/post-filter.component';
import {CoverComponent} from './cover/cover.component';
// Componente de login----

import {LoginComponent} from './components/login.component';



const appRoutes: Routes=[

    {path: '', component:CoverComponent},
    {path:'home/:page',component:HomeComponent},
    {path:'post-detalle',component:PostDetailsComponent},
    {path: 'admin', component:AdminComponent},
    {path: 'perfiladd', component:PerfilAddComponent},
    {path:'updateperfil/:id',component:PerfileditComponent},
    {path:'educacionadd',component:EducacionAddComponent},
    {path:'updateeducacion/:id',component:EducacioneditComponent},
    {path: 'sobremi',component:SobremiComponent},
    {path:'portafolio',component:PortafolioComponent},
    {path:'portafolioadd',component:PortafolioAddComponent},
    {path:'updateportafolio/:id',component:PortafolioeditComponent},
    //Rutas para page service
    {path:'service',component:ServiceComponent},
    //Rutas para contactos
    {path:'contacto',component:ContactoComponent},
    //Rutas de Post
    {path:'postdetails/:slug',component:PostDetailsComponent},
    {path:'postadd',component:PostAddComponent},
    {path:'postedit/:slug',component:PosteditComponent},
    {path:'SearchPost/:search',component:PostFilterComponent},
    // Categoria
    {path:'categoria/:id',component:CategoriaComponent},

    {path:'',redirectTo:'/home',pathMatch:'full'},

    {path: 'home',component:AppComponent},

    {path:'register',component:RegisterComponent},
    {path:'login',component:LoginComponent},
    {path:'userslist',component:UserListComponent},
    {path:'useredit/:id',component:UserEditComponent}
];

export const appRoutingProviders: any[]=[];
export const routing: ModuleWithProviders= RouterModule.forRoot(appRoutes);
