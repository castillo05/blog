import { Categoria } from './../models/categoria';
import { CategoriaService } from './../service/categoria.service';
import { UserService } from './../service/user.service';
import { GLOBAL } from './../service/global';
import { User } from './../models/user';
import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Post } from '../models/post';
import { PostService } from '../service/post.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'post-add',
    //template:' <ckeditor [(ngModel)]="content" debounce="500"> <p>Hello <strong>world</strong></p> </ckeditor> <div [innerHTML]="content"></div>',
    templateUrl:'../views/post-add.html',
    styleUrls: ['../css.component/post-add.component.css'],
    providers:[UserService,PostService,CategoriaService]
})

export class PostAddComponent implements OnInit{
	public titulo:string;
    public user:User;
    public post:Post;
    public categoria:Categoria[];
    public cat:Categoria;
    public identity;
    public token;
    public url:String;
    public errorMessage;
    public alertMessage;
    public add=true;
    public chek:boolean;
    public closeResult: string;


	constructor(
		private _route: ActivatedRoute,
        private _router: Router,
        private _userService:UserService,
        private _postService:PostService,
        private _categoriaService:CategoriaService,
        private modalService: NgbModal
	

	){
        this.titulo= 'Guardar Post';
        this.identity=this._userService.getIdentity();
        this.token=this._userService.getToken();
        this.user= new User('','','','','','','','');
        this.post= new Post('','','','','','','','','','');
        this.cat=new Categoria('','');
        this.url=GLOBAL.url;
        this.chek=true;
        
	}

	ngOnInit()
	{
		console.log('PostAddComponent cargado');
		//Consegur el listado de artista
        this.identity=this._userService.getIdentity();
        this.token=this._userService.getToken();
        this.post.description='<h1>Hola Mundo</h1>';
        this.post.user=this.identity._id;
        //console.log(this.identity.name);
        
        console.log(this.post);
        
        this.getCategoria();

    }


    // Modal
    open(content) {
        this.modalService.open(content).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
      }
    
      private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
          return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
          return 'by clicking on a backdrop';
        } else {
          return  `with: ${reason}`;
        }
      }
    // Fin

    saveCat(){
        this._categoriaService.savecategoria(this.cat).subscribe(response=>{
            if(response.message){
                return alert(response.message);
            } 
            if (!response.cat) {
                alert(response.message);
            } else {
                this.cat=response.cat;
                this.getCategoria();
                
            }
        },error=>{
            var errorMessage = <any>error;

            var body = JSON.parse(error._body);
            this.alertMessage = body.message;
            if (errorMessage !=null) { 
            this.alertMessage=body.message; 
            console.log(error);
            }
        }
    );
    }
    getCategoria(){
        this._categoriaService.getCategoria(this.token).subscribe(response=>{
            if (!response.categoria) {
                console.log('No Hay Categoria');
            } else {
                
                this.categoria=response.categoria;
                //console.log(response.categoria);
            }
        },
        error=>{
                var errorMessage = <any>error;

                var body = JSON.parse(error._body);
                this.alertMessage = body.message;
                if (errorMessage !=null) { 
                this.alertMessage=body.message; 
                console.log(error);
                }
            });
    
    }
    
    onSubmit(){
        
         if(!this.post.idvideo){return console.log('No hay video de youtube.com...');}
         this._postService.savePost(this.token,this.post).subscribe(response=>{
            
                if (response.post) {
                     this.alertMessage=('El Post se ha agregado correctamente');
                    //this.post=response.post;
                    console.log(response.post);
                    this._router.navigate(['/postdetails/'+response.post.slug]);
                } else {
                   
                    this.alertMessage=('Error en el servidor');

                }
            },
			error=>{
					var errorMessage = <any>error;

	                var body = JSON.parse(error._body);
	                this.alertMessage = body.message;
	                if (errorMessage !=null) { 
					this.alertMessage=body.message; 
					console.log(error);
					}
				});
    }


    

    }

    