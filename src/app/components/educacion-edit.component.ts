import { Educacion } from './../models/educacion';
import { Perfil } from './../models/perfil';
import { UserService } from './../service/user.service';
import { GLOBAL } from './../service/global';
import { User } from './../models/user';
import{Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { PerfilService } from '../service/perfil.service';
import { EducacionService } from '../service/educacion.service';



@Component({
	selector: 'educacion',
    templateUrl:'../views/educacion.html',
    styleUrls: ['../css.component/admin.component.css'],
    providers:[UserService,PerfilService,EducacionService]
})

export class EducacioneditComponent implements OnInit{
	public titulo:string;
    public user:User;
    public perfil:Perfil;
    public educacion:Educacion;
    public identity;
    public token;
    public url:String;
    public errorMessage;
    public alertMessage;

    public chek=false;


	constructor(
		private _route: ActivatedRoute,
        private _router: Router,
        private _userService:UserService,
        private _perfilService:PerfilService,
        private _educacionService:EducacionService
	

	){
		this.titulo= 'Editar Educacion';
        this.user= new User('','','','','','','','');
        this.perfil=new Perfil('','','','','','');
        this.educacion=new Educacion('','','','','','');
        this.url=GLOBAL.url;
	}

	ngOnInit()
	{
		console.log('AdminComponent cargado');
		//Consegur el listado de artista
        this.identity=this._userService.getIdentity();
        this.token=this._userService.getToken();

        console.log(this.identity);
        console.log(this.token);
        this.geteducacionone();
        if(this.identity.roles.roles!='administrador'){
            this._router.navigate(['/']);
        }

    }

    geteducacionone(){
        this._route.params.forEach((params:Params)=>{
            let id=params['id'];
            this._educacionService.getEducacionOne(this.token,id).subscribe(response=>{
                if (!response.educacion) {
                    alert('Error...')
                } else {
                    this.educacion=response.educacion;
                }
            },
            error=>{
                        var errorMessage = <any>error;

                    var body = JSON.parse(error._body);
                    this.alertMessage = body.message;
                    if (errorMessage !=null) { 
                    this.alertMessage=body.message; 
                    console.log(error);
                    }
                });
        });
    }
    
    onSubmit(){
       
        this._route.params.forEach((params:Params)=>{
            let id=params['id'];
        this._educacionService.updateEducacion(this.token,id,this.educacion).subscribe(
            response=>{
                
                if (!response.educacion) { 
                    this.alertMessage=('Error en el servidor');
                } else {
                    this.alertMessage=('El Elemento se ha actualizado correctamente');
                    //this.artist=response.artist;
                    //this._router.navigate(['/editar-artista']),response.artist._id);
                    //Subir la imagen
                    // if (!this.filesToUpload) { 
                    //     this._router.navigate(['/artista',response.artist._id]);
                    // } else {
                    //         this._UploadService.makeFileRequest(this.url+'upload-image-artist/'+id,[],this.filesToUpload,this.token,'image')
                    //     .then(
                    //             (result)=>{
                    //                 this._router.navigate(['/artists',1]);
                    //             },
                    //             (error)=>{
                    //                 console.log(error);
                    //             }
                    //         );
                    // }
                    this._router.navigate(['/sobremi']);
                    
                }
            },
            error=>{
                    var errorMessage = <any>error;

                    var body = JSON.parse(error._body);
                    this.alertMessage = body.message;
                    if (errorMessage !=null) { 
                    this.alertMessage=body.message; 
                    console.log(error);
                    }
                }
            );
    });
}

   


}