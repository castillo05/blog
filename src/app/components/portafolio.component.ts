import { Educacion } from './../models/educacion';
import { PerfilService } from './../service/perfil.service';
import { Perfil } from './../models/perfil';
import { UserService } from './../service/user.service';
import { GLOBAL } from './../service/global';
import { User } from './../models/user';
import{Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { ParamMap } from '@angular/router/src/shared';
import { EducacionService } from '../service/educacion.service';
import { PortafolioService } from '../service/portafolio.service';
import { Portafolio } from '../models/portafolio';


@Component({
	selector: 'portafolio',
    templateUrl:'../views/portafolio.html',
    styleUrls:['../css.component/portafolio.component.css'],
    providers:[UserService,PerfilService,EducacionService,PortafolioService]
})

export class PortafolioComponent implements OnInit{
	public titulo:string;
    public user:User;
    public perfils:Perfil[];
    public educacions:Educacion[];
    public portafolios:Portafolio[];
    public identity;
    public token;
    public url:String;
    public errorMessage;
    public alertMessage;
    public next_page;
	public prev_page;

	constructor(
		private _route: ActivatedRoute,
        private _router: Router,
        private _userService:UserService,
        private _perfilService:PerfilService,
        private _educacionService:EducacionService,
        private _portafolioService:PortafolioService
	

	){
		this.titulo= 'Sobre Mi';
        this.user= new User('','','','','','','','');
        this.url=GLOBAL.url;
        this.next_page=1;
		this.prev_page=1;
	}

	ngOnInit()
	{
		console.log('AdminComponent cargado');
		//Consegur el listado de artista
        this.identity=this._userService.getIdentity();
        this.token=this._userService.getToken();

        console.log(this.identity);
        console.log(this.token);
        this.getportafolios();
        // this.getEducacion();
        

    }

    getportafolios(){
        // this._route.params.forEach((params:Params)=>{
        //     let page=+params['page'];
        //     if (!page) {
        //         page=1;
        //     } else {
        //         this.next_page=page+1;
		// 		this.prev_page=page-1;

		// 		if (this.prev_page==0) {
		// 			this.prev_page=1;
        //         }
        //     }
        
        this._portafolioService.getPortafolios(this.token).subscribe(
            response=>{
            if (!response.portafolios) {
                alert('No hay portafolios agregadas');
            } else {
                this.portafolios=response.portafolios;
            }
        },error=>{
            var errorMessage = <any>error;
            
            var body = JSON.parse(error._body);
            this.alertMessage = body.message;
            if (errorMessage !=null) { 
            this.alertMessage=body.message; 
            console.log(error);
            }
        });

    }


    // getEducacion(){
        
        
    //     this._educacionService.getEducacion(this.token).subscribe(
    //         response=>{
    //         if (!response.educacions) {
    //             alert('No hay educacion agregadas');
    //         } else {
    //             this.educacions=response.educacions;
    //         }
    //     },error=>{
    //         var errorMessage = <any>error;
            
    //         var body = JSON.parse(error._body);
    //         this.alertMessage = body.message;
    //         if (errorMessage !=null) { 
    //         this.alertMessage=body.message; 
    //         console.log(error);
    //         }
    //     });

    // }

    onDeletePortafolio(id){
        this._portafolioService.deletePortafolio(this.token,id).subscribe(response=>{
            if (!response.portafolio) {
                alert('Error el el server');
            } else {
                this.getportafolios();
            }
        },
        error=>{
                    var errorMessage = <any>error;

                var body = JSON.parse(error._body);
                this.alertMessage = body.message;
                if (errorMessage !=null) { 
                this.alertMessage=body.message; 
                console.log(error);
                }
            });
    }


    // onDeleteEducacion(id){
    //     this._educacionService.deleteEducacion(this.token,id).subscribe(response=>{
    //         if (!response.educacion) {
    //             alert('Error el el server');
    //         } else {
    //             this.getEducacion();
    //         }
    //     },
    //     error=>{
    //                 var errorMessage = <any>error;

    //             var body = JSON.parse(error._body);
    //             this.alertMessage = body.message;
    //             if (errorMessage !=null) { 
    //             this.alertMessage=body.message; 
    //             console.log(error);
    //             }
    //         });
    // }



}
