import { GLOBAL } from './../service/global';
import { PostService } from './../service/post.service';
import { UserService } from './../service/user.service';
import{Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Post } from '../models/post';
import { FacebookService, InitParams } from 'ngx-facebook';
import { Meta, Title } from '@angular/platform-browser';


@Component({
	selector: 'post-details',
    templateUrl:'../views/post-details.html',
    styleUrls: ['../css.component/post-details.component.css'],
	providers:[UserService,PostService]
})

export class PostDetailsComponent implements OnInit{
    public titulo:string;
    public publicacion:Post;
	public identity;
    public token;
    public next_page;
	public prev_page;
	public alertmessage;
    public url;

    private id;
    private player;
    private ytEvent;



	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
        private _userService:UserService,
        private _postService:PostService,
        private fb: FacebookService,
        private meta:Meta

	){
        this.titulo= 'Publicaciones';
        this.next_page=1;
		this.prev_page=1;
        this.url=GLOBAL.url;

        this.getPostOne();

      

        let params: InitParams = {
            appId: '1399875313450062',
            xfbml: true,
            version: 'v2.8'
              };
            
              fb.init(params);
	}

	ngOnInit()
	{
		console.log('PostListComponent cargado');
		//Consegur el listado de artista
		this.identity=this._userService.getIdentity();
		this.token=this._userService.getToken();

		// console.log(this.identity);
		// console.log(this.token);
       
        
    }

    onStateChange(event) {
        this.ytEvent = event.data;
      }
      savePlayer(player) {
        this.player = player;
      }
      
      playVideo() {
        this.player.playVideo();
      }
      
      pauseVideo() {
        this.player.pauseVideo();
      }
    
    getPostOne(){
        this._route.params.forEach((params:Params)=>{
            let slug = params['slug'];

            this._postService.getPostsOne(slug).subscribe(response=>{

                if (!response.publicacion) {
                    this._router.navigate(['/']);
                } else {
                    this.publicacion=response.publicacion;
                    // console.log(this.publicacion);
                    this.titulo=this.publicacion.title; 
                   
        // Meta
        this.meta.updateTag({name:'title',content:this.publicacion.title});
        this.meta.updateTag({ name: 'description', content: this.publicacion.resume});
        this.meta.updateTag({ name: 'author', content: 'Jorge Castillo Moreno' });
        this.meta.updateTag({ name: 'keywords', content: 'Jorge Castillo, Informatico, Programador Nicaragua, Tecnologia Nicaragua' });
        this.meta.updateTag({property:'og:title', content:this.publicacion.title});
        this.meta.updateTag({property:'og:type', content:'artículo'});
        this.meta.updateTag({property:'og:description', content:this.publicacion.resume});
        this.meta.updateTag({property:'og:url', content:'http://www.jorgecastillodeveloper.pro/sobremi'});
        this.meta.updateTag({property:'og:image', content:this.url+'get-image-post/'+this.publicacion.image});
        this.meta.updateTag({property:'og:fb:admins', content:'https://www.facebook.com/jorgeacastillomoreno/'});
        // fin
        
                }
            },error=>{
                var errorMessage = <any>error;
                
                var body = JSON.parse(error._body);
                this.alertmessage = body.message;
                if (errorMessage !=null) { 
                this.alertmessage=body.message; 
                console.log(error);
                }
            }
        
        );

        });
    }


    onDeletePost(id){
		this._postService.deletePost(this.token,id).subscribe(response=>{
			if (!response.post) {
				alert('Error el el server');
			} else {
                // this.getPost();
                this._router.navigate(['/']);
			}
		}, error=>{
			var errorMessage = <any>error;

		var body = JSON.parse(error._body);
		this.alertmessage = body.message;
		if (errorMessage !=null) { 
		this.alertmessage=body.message; 
		console.log(error);
		}
	}
	);
	}

}