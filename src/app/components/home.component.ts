import { GLOBAL } from './../service/global';
import { PostService } from './../service/post.service';
import { UserService } from './../service/user.service';
import{Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { User } from '../models/user';
import { Post } from '../models/post';
import { Meta } from '@angular/platform-browser';
import { NotificationsService } from 'angular2-notifications';
import { AlertsService, AlertType} from '@jaspero/ng2-alerts';



@Component({
	selector: 'home',
	templateUrl:'../views/home.html',
	//template:' <jaspero-alerts [defaultSettings]="options"></jaspero-alerts>',
	styleUrls: ['../css.component/home.component.css'],
	providers:[UserService,PostService]
})

export class HomeComponent implements OnInit{
	public titulo:string;
    public posts:Post[];
    public publication:Post[];
	public identity;
    public token;
    public next_page;
	public prev_page;
	public alertmessage;
    public url;
    public page;
    public u:boolean;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService:UserService,
		private _postService:PostService,
        private meta:Meta,
        private _service : NotificationsService,
        private _alert: AlertsService
	){
		this.titulo= 'jorgecastillodeveloper';
		this.next_page=1;
        this.prev_page=1;
        this.page=4;
        this.url=GLOBAL.url;
        this.u=true;
        
        this.meta.updateTag({ name: 'author', content: 'Jorge Castillo Moreno' });
        this.meta.updateTag({ name: 'keywords', content: 'Jorge Castillo, Informatico, Programador Nicaragua, Tecnologia Nicaragua' });
        this.meta.updateTag({property:'og:title', content:'Jorge Castillo JC Developer Nicaragua Articulos Tutoriales sobre tecnologias'});
        this.meta.updateTag({property:'og:type', content:'artículo'});
        this.meta.updateTag({property:'og:description', content:'JC Developer Nicaragua te ofrece los mejores servicios en el desarrollo de tu sitio web, aplicacion y lo que necesites en el desarrollo de tu negocio a la par de la tecnologia'});
        this.meta.updateTag({property:'og:url', content:'http://www.jorgecastillodeveloper.pro/sobremi'});
        this.meta.updateTag({property:'og:image', content:'http://www.jorgecastillodeveloper.pro/assets/img/jorge.jpg'});

        this.meta.updateTag({property:'og:fb:admins', content:'https://www.facebook.com/jorgeacastillomoreno/'});

        this.meta.updateTag({ name: 'description', content: 'Hola soy jorge castillo y este es mi blog en donde publico tutoriales, articulos y muchas cosas mas sobre tecnologia y mas' });
        
        
	}

	ngOnInit()
	{
		//console.log('HomeComponent cargado');
		//Consegur el listado de artista
		this.identity=this._userService.getIdentity();
		this.token=this._userService.getToken();
		
		//console.log(this.identity);
		//console.log(this.token);
        this.getPost();
       this.getPostEnd();
		this.open('success');

    }    
    open(type:AlertType) {
        this._alert.create(type, 'This is a title');
    }

	getPostEnd(){
       

            this._postService.endPost().subscribe(response=>{
                if (!response.publication) {
                    this._router.navigate(['/']);
                } else { 
                    //console.log(response.publication);
                    this.publication=response.publication;
                   
                }
            },error=>{
                var errorMessage = <any>error;
                
                var body = JSON.parse(error._body);
                this.alertmessage = body.message;
                if (errorMessage !=null) { 
                this.alertmessage=body.message; 
                console.log(error);
                }
            }
        
        );

        }

	getPost(){
        this._route.params.forEach((params:Params)=>{
            let page=+params['page'];
            if (!page) {
                page=2  
            } else {
                this.page=page+2;
				
                

            }
        

            this._postService.getPosts(this.token,page).subscribe(response=>{
                if (!response.posts) {
                    this._router.navigate(['/']);
                } else {
                    
                    this.posts=JSON.parse(JSON.stringify(response.posts));
                    this.u=false;
                }
            },error=>{
                var errorMessage = <any>error;
                
                var body = JSON.parse(error._body);
                this.alertmessage = body.message;
                if (errorMessage !=null) { 
                this.alertmessage=body.message; 
                console.log(error);
                }
            }
        
        );

         });
	}
	
	

}