import { UploadService } from './../service/upload.service';
import { PortafolioService } from './../service/portafolio.service';
import { Portafolio } from './../models/portafolio';
import { Educacion } from './../models/educacion';
import { Perfil } from './../models/perfil';
import { UserService } from './../service/user.service';
import { GLOBAL } from './../service/global';
import { User } from './../models/user';
import{Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { PerfilService } from '../service/perfil.service';
import { EducacionService } from '../service/educacion.service';



@Component({
	selector: 'portafolio-add',
    templateUrl:'../views/portafolio-add.html',
    styleUrls: ['../css.component/admin.component.css'],
    providers:[UserService,PerfilService,EducacionService,PortafolioService,UploadService]
})

export class PortafolioeditComponent implements OnInit{
	public titulo:string;
    public user:User;
    public perfil:Perfil;
    public educacion:Educacion;
    public portafolio:Portafolio;
    public identity;
    public token;
    public url:String;
    public errorMessage;
    public alertMessage;

    public is_edit=true;


	constructor(
		private _route: ActivatedRoute,
        private _router: Router,
        private _userService:UserService,
        private _perfilService:PerfilService,
        private _educacionService:EducacionService,
        private _portafolioService:PortafolioService,
        private _uploadService:UploadService
	

	){
		this.titulo= 'Editar Portafolio';
        this.user= new User('','','','','','','','');
        this.perfil=new Perfil('','','','','','');
        this.educacion=new Educacion('','','','','','');
        this.portafolio=new Portafolio('','','','','','');
        this.url=GLOBAL.url;
	}

	ngOnInit()
	{
		console.log('AdminComponent cargado');
		//Consegur el listado de artista
        this.identity=this._userService.getIdentity();
        this.token=this._userService.getToken();

        console.log(this.identity);
        console.log(this.token);
        this.getportafolioone();
        if(this.identity.roles.roles!='administrador'){
            this._router.navigate(['/']);
        }
    }

    getportafolioone(){
        this._route.params.forEach((params:Params)=>{
            let id=params['id'];
            this._portafolioService.getPortafolioone(this.token,id).subscribe(response=>{
                if (!response.portafolio) {
                    console.log(response.portafolio);
                } else {
                    this.portafolio=response.portafolio;
                }
            },
            error=>{
                        var errorMessage = <any>error;

                    var body = JSON.parse(error._body);
                    this.alertMessage = body.message;
                    if (errorMessage !=null) { 
                    this.alertMessage=body.message; 
                    console.log(error);
                    }
                });
        });
    }
    
    onSubmit(){
       
        this._route.params.forEach((params:Params)=>{
            let id=params['id'];
        this._portafolioService.updatePortafolio(this.token,id,this.portafolio).subscribe(
            response=>{
                
                if (!response.portafolio) { 
                    this.alertMessage=('Error en el servidor');
                } else {
                    alert('El Elemento se ha actualizado correctamente');
                    this.portafolio=response.portafolio;
                    // this._router.navigate(['/editar-artista']),response.artist._id);
                    // Subir la imagen
                    if (!this.filesToUpload) { 
                        // this._router.navigate(['/portafolio',response.portafolio._id]);
                        this._router.navigate(['/portafolio']);
                    } else {

                         this._uploadService.makeFileRequest(this.url+'upload-image-portafolio/'+id,[],this.filesToUpload,this.token,'image')
                        .then(
                           
                               (result)=>{
                                    // this._router.navigate(['/portafolio',1]);
                                    this._router.navigate(['/portafolio']);
                                },
                                (error)=>{
                                    console.log(error);
                                }
                            ); 
                           
                    }
                    
                    
                }
            },
            error=>{
                    var errorMessage = <any>error;

                    var body = JSON.parse(error._body);
                    this.alertMessage = body.message;
                    if (errorMessage !=null) { 
                    this.alertMessage=body.message; 
                    console.log(error);
                    }
                }
            );
    });
}


public filesToUpload:Array<File>;

fileChangeEvent(fileInput:any){
    this.filesToUpload=<Array<File>>fileInput.target.files;
}

   


}