import { UploadService } from './../service/upload.service';
import { UserService } from './../service/user.service';
import { GLOBAL } from './../service/global';
import { User } from './../models/user';
import{Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { PostService } from '../service/post.service';
import { Post } from '../models/post';
import { Alert } from 'selenium-webdriver';
import { Categoria } from './../models/categoria';
import { CategoriaService } from './../service/categoria.service';




@Component({
	selector: 'post-add',
    templateUrl:'../views/post-add.html',
    providers:[UserService,UploadService,PostService,CategoriaService]
})

export class PosteditComponent implements OnInit{
	public titulo:string;
    public user:User;
    public post:Post;
    public categoria:Categoria[];
    public identity;
    public token;
    public url:String;
    public errorMessage;
    public alertMessage;
    public width;
    public height;
    public is_edit=true;
    public u:boolean;


	constructor(
		private _route: ActivatedRoute,
        private _router: Router,
        private _userService:UserService,
        private _postService:PostService,
        private _uploadService:UploadService,
        private _categoriaService:CategoriaService
	

	){
		this.titulo= 'Editar Post';
        this.user= new User('','','','','','','','');
        this.post= new Post('','','','','','','','','','');
        this.url=GLOBAL.url;
        

	}

	ngOnInit()
	{
		console.log('posteditComponent cargado');
		//Consegur el listado de artista
        this.identity=this._userService.getIdentity();
        this.token=this._userService.getToken();
        
        console.log(this.identity);
        console.log(this.token);
        this.getpostone();
        this.getCategoria();
        
       
    }

    

    getCategoria(){
        this._categoriaService.getCategoria(this.token).subscribe(response=>{
            if (!response.categoria) {
                console.log('No Hay Categoria');
            } else {
                this.categoria=response.categoria;
                //console.log(response.categoria);
            }
        },
        error=>{
                var errorMessage = <any>error;

                var body = JSON.parse(error._body);
                this.alertMessage = body.message;
                if (errorMessage !=null) { 
                this.alertMessage=body.message; 
                console.log(error);
                }
            });
    
    }

    getpostone(){
        this._route.params.forEach((params:Params)=>{
            let slug=params['slug'];
            this._postService.getPostsOneEdit(this.token,slug).subscribe(response=>{
                if (response.post) {
                    console.log(response.publicacion);
                } else {
                    this.post=response.publicacion;
                    if (this.identity._id!=this.post.user && this.identity.roles.roles!='administrador') {
                       return this._router.navigate(['/']);
                    }
                   
                }
            },
            error=>{
                        var errorMessage = <any>error;

                    var body = JSON.parse(error._body);
                    this.alertMessage = body.message;
                    if (errorMessage !=null) { 
                    this.alertMessage=body.message; 
                    console.log(error);
                    }
                });
        });
    }
    
    onSubmit(){
        console.log(this.post);
        this._route.params.forEach((params:Params)=>{
            let id=params['id'];
           
        this._postService.updatePost(this.token,this.post._id,this.post).subscribe(
            response=>{
                
                if (!response.post) { 
                    this.alertMessage=('Error en el servidor');
                } else {
                    alert('El Elemento se ha actualizado correctamente');
                    this.post=response.post;
                    // this._router.navigate((['/postdetails']),response.post._id);
                    // Subir la imagen
                    if (!this.filesToUpload) { 
                        // this._router.navigate(['/portafolio',response.portafolio._id]);
                        //this._router.navigate(['/']);
                        this._router.navigate(['/postdetails/'+response.post.slug]);
                    } else {

                         this._uploadService.makeFileRequest(this.url+'upload-image-post/'+this.post._id,[],this.filesToUpload,this.token,'image')
                        .then(
                           
                               (result)=>{
                                this.u=false;
                                    // this._router.navigate(['/portafolio',1]);
                                    this._router.navigate(['/postdetails/'+response.post.slug]);
                                },
                                (error)=>{
                                    console.log(error);
                                }
                            ); 

                            
                           
                    }
                    
                    
                }
            },
            error=>{
                    var errorMessage = <any>error;

                    var body = JSON.parse(error._body);
                    this.alertMessage = body.message;
                    if (errorMessage !=null) { 
                    this.alertMessage=body.message; 
                    console.log(error);
                    }
                }
            );
    });
}

UploadVideo(){
    this.u=true;
    this._route.params.forEach((params:Params)=>{
        let id=params['id'];
    this._postService.updatePost(this.token,this.post._id,this.post).subscribe(
        response=>{
            
            if (!response.post) { 
                this.alertMessage=('Error en el servidor');
            } else {
                alert('El Elemento se ha actualizado correctamente');
                this.post=response.post;
                // this._router.navigate((['/postdetails']),response.post._id);
                // Subir la imagen
                if (!this.filesToUpload) { 
                    // this._router.navigate(['/portafolio',response.portafolio._id]);
                    //this._router.navigate(['/']);
                    this._router.navigate(['/postdetails/'+response.post._id]);
                } else {

                    
                    this._uploadService.makeFileRequest(this.url+'upload-video-post/'+this.post._id,[],this.filesToUpload,this.token,'video')
                    .then(

                        
                       
                           (result)=>{
                               this.u=false;
                                // this._router.navigate(['/portafolio',1]);
                                console.log(result);
                                //this.u=true;
                                this._router.navigate(['/postdetails/'+response.post.slug]);
                            },
                            (error)=>{
                                console.log(error);
                            }
                        ); 
                        

                        
                       
                }
                
                
            }
        },
        error=>{
                var errorMessage = <any>error;

                var body = JSON.parse(error._body);
                this.alertMessage = body.message;
                if (errorMessage !=null) { 
                this.alertMessage=body.message; 
                console.log(error);
                }
            }
        );
});
}


public filesToUpload:Array<File>;

fileChangeEvent(fileInput:any,obj){
    var uploadFile = fileInput.target.files[0];
    
    if (!(/\.(jpg|png|gif)$/i).test(uploadFile.name)) {
        alert('El archivo a adjuntar no es una imagen');
        this.filesToUpload=null;
    }else{
       
        this.filesToUpload=<Array<File>>fileInput.target.files;
    }
   
}

public filesToUpload2:Array<File>;

fileChangeEvent2(fileInput:any){
    var uploadFile = fileInput.target.files[0];
    
    if (!(/\.(mp4)$/i).test(uploadFile.name)) {
        alert('El archivo a adjuntar no es un video');
        this.filesToUpload=null;
    }else{
        
      this.filesToUpload=<Array<File>>fileInput.target.files
         
    }
}

   


}