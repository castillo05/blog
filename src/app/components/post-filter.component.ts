import { Post } from '../models/post';
import { UserService } from '../service/user.service';
import { GLOBAL } from '../service/global';
import { User } from '../models/user';
import { Component,OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { FacebookService, LoginResponse, LoginOptions, UIResponse, UIParams, FBVideoComponent } from 'ngx-facebook';
import { Categoria } from '../models/categoria';
import { CategoriaService } from '../service/categoria.service';
import { PostService } from '../service/post.service';
import { Meta } from '@angular/platform-browser';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-root',
  templateUrl: '../views/postfilter.html',
  styleUrls: ['../app.component.css'],
  providers:[UserService,CategoriaService,PostService]
})
export class PostFilterComponent implements OnInit {
 
  title = 'jorgecastillodevelop.pro';
  public identity;
  public token;
  public posts:Post[];
  public user:User;
  public postcat:Post;
  public cat:Categoria;
  public url:String;
  public errorMessage;
  public alertMessage;
  public on;
  public categoria:Categoria[];
  public page=2;
  public closeResult: string;
  search:String;  


  constructor(
    private _userService:UserService,
    private _route:ActivatedRoute,
    private _router:Router,
    private fb: FacebookService,
    private _categoriaService:CategoriaService,
    private _postService:PostService,
    private meta:Meta,
    private modalService: NgbModal

  )
  
  {
    this.user= new User('','','','','','','','');
    this.url=GLOBAL.url;
   
    

        this.meta.updateTag({ name: 'description', content: 'Hola somos codigo nica y en este blog en donde publicamos tutoriales, articulos y muchas cosas mas sobre tecnologia y mas' });
        this.meta.updateTag({ name: 'author', content: 'Jorge Castillo Moreno' });
        this.meta.updateTag({ name: 'keywords', content: 'Jorge Castillo, Informatico, Programador Nicaragua, Tecnologia Nicaragua' });
        this.meta.updateTag({property:'og:title', content:'Jorge Castillo JC Developer Nicaragua Articulos Tutoriales sobre tecnologias'});
        this.meta.updateTag({property:'og:type', content:'artículo'});
        this.meta.updateTag({property:'og:description', content:'JC Developer Nicaragua te ofrece los mejores servicios en el desarrollo de tu sitio web, aplicacion y lo que necesites en el desarrollo de tu negocio a la par de la tecnologia'});
        this.meta.updateTag({property:'og:url', content:'http://www.jorgecastillodeveloper.pro'});
        this.meta.updateTag({property:'og:image', content:'http://www.jorgecastillodeveloper.pro/assets/img/jorge.jpg'});

        this.meta.updateTag({property:'og:fb:admins', content:'https://www.facebook.com/jorgeacastillomoreno/'});
  }
  ngOnInit(): void {
      this.identity=this._userService.getIdentity();
      this.token=this._userService.getToken();
     
      this.SearchPost();
    }

   

   
    SearchPost(){
        this._route.params.forEach((params:Params)=>{
            this.search=params['search'];
            if(this.search==''){
                this._router.navigate(['/']);
            }
            // let page=+params['page'];
            // if (!page) {
            //     page=2  
            // } else {
            //     this.page=page+2;

            // }
        

            this._postService.getsearchpost(this.search).subscribe(response=>{
                if (!response.searchPost) {
                    this._router.navigate(['/']);
                }else {
                    
                    this.posts=response.searchPost;
                    // console.log(response.searchPost );
                }
            },error=>{
                var errorMessage = <any>error;
                
                var body = JSON.parse(error._body);
                this.alertMessage = body.message;
                if (errorMessage !=null) { 
                this.alertMessage=body.message; 
                console.log(error);
                }
            }
        
        );

        });
        
        
    }


}
