import { UserService } from '../service/user.service';
import { GLOBAL } from '../service/global';
import { User } from '../models/user';
import { Component,OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { FacebookService, LoginResponse, LoginOptions, UIResponse, UIParams, FBVideoComponent } from 'ngx-facebook';
import { Meta } from '@angular/platform-browser';


@Component({
  selector: 'home',
  templateUrl: '../views/register.html',
  providers:[UserService]
})
export class RegisterComponent implements OnInit {
 
  title = 'jorgecastillodevelop.pro';
  public identity;
  public token;
  //public posts:Post[];
  public userR:User;
  //public postcat:Post;
  public url:String;
  public errorMessage;
  public alertMessage;
  public on;
 // public categoria:Categoria[];
 // public page=2;
 public alertRegister;

  constructor(
    private _userService:UserService,
    private _route:ActivatedRoute,
    private _router:Router,
    private fb: FacebookService,
    private meta:Meta

  )
  
  {
    this.userR= new User('','','','','','5a32b2478f27c41d78a7946f','','');
    this.url=GLOBAL.url;
   
    

    this.meta.updateTag({ name: 'description', content: 'Hola soy jorge castillo y este es mi blog en donde publico tutoriales, articulos y muchas cosas mas sobre tecnologia y mas' });
        this.meta.updateTag({ name: 'author', content: 'Jorge Castillo Moreno' });
        this.meta.updateTag({ name: 'keywords', content: 'Jorge Castillo, Informatico, Programador Nicaragua, Tecnologia Nicaragua' });
        this.meta.updateTag({property:'og:title', content:'Jorge Castillo JC Developer Nicaragua Articulos Tutoriales sobre tecnologias'});
        this.meta.updateTag({property:'og:type', content:'artículo'});
        this.meta.updateTag({property:'og:description', content:'JC Developer Nicaragua te ofrece los mejores servicios en el desarrollo de tu sitio web, aplicacion y lo que necesites en el desarrollo de tu negocio a la par de la tecnologia'});
        this.meta.updateTag({property:'og:url', content:'http://www.jorgecastillodeveloper.pro'});
        this.meta.updateTag({property:'og:image', content:'http://www.jorgecastillodeveloper.pro/assets/img/jorge.jpg'});

        this.meta.updateTag({property:'og:fb:admins', content:'https://www.facebook.com/jorgeacastillomoreno/'});
  }
  ngOnInit(): void {
      this.identity=this._userService.getIdentity();
      this.token=this._userService.getToken();
     
    }

   
   

   
   


    

    onSubmit(){
      this._userService.register(this.userR).subscribe(response=>{
          let identity=response.user;
          this.identity=identity;
          //console.log(this.identity);
          if (response.message) {
              return alert(response.message);
          } 
          if (!this.identity._id) {
              alert('El Usuario no esta correctamente logeado');
              console.log(this.identity);
          }else{
                      
                this.userR = new User('','','','','','','','');
                alert('Registro Satisfactorio...Te hemos enviado un correo a '+response.user.email);
            }
               
              
         
              
          
      },error=>{
          var errorMessage=<any>error;
          var body=JSON.stringify(error._body);
          
          if (errorMessage!=null) {
              this.errorMessage=body;
              console.log(error);
              alert(this.errorMessage);
          }
      }
  );
  }

 

}
