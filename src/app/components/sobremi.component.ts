import { Educacion } from './../models/educacion';
import { PerfilService } from './../service/perfil.service';
import { Perfil } from './../models/perfil';
import { UserService } from './../service/user.service';
import { GLOBAL } from './../service/global';
import { User } from './../models/user';
import{Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { ParamMap } from '@angular/router/src/shared';
import { EducacionService } from '../service/educacion.service';
import { FacebookService, InitParams } from 'ngx-facebook';
import { Meta } from '@angular/platform-browser';

@Component({
	selector: 'sobremi',
    templateUrl:'../views/sobremi.html',
    styleUrls:['../css.component/sobremi.component.css'],
    providers:[UserService,PerfilService,EducacionService]
})

export class SobremiComponent implements OnInit{
	public titulo:string;
    public user:User;
    public perfils:Perfil[];
    public educacions:Educacion[];
    public identity;
    public token;
    public url:String;
    public errorMessage;
    public alertMessage;
    public next_page;
	public prev_page;

	constructor(
		private _route: ActivatedRoute,
        private _router: Router,
        private _userService:UserService,
        private _perfilService:PerfilService,
        private _educacionService:EducacionService,
        private fb: FacebookService,
        private meta:Meta
	

	){
		this.titulo= 'Sobre Mi';
        this.user= new User('','','','','','','','');
        this.url=GLOBAL.url;
        this.next_page=1;
        this.prev_page=1;
        // Meta
        this.meta.updateTag({ name: 'description', content: 'Hola soy jorge castillo y este es mi blog en donde publico tutoriales, articulos y muchas cosas mas sobre tecnologia y mas' });
        this.meta.updateTag({ name: 'author', content: 'Jorge Castillo Moreno' });
        this.meta.updateTag({ name: 'keywords', content: 'Jorge Castillo, Informatico, Programador Nicaragua, Tecnologia Nicaragua' });
        this.meta.updateTag({property:'og:title', content:'Jorge Castillo JC Developer Nicaragua Articulos Tutoriales sobre tecnologias'});
        this.meta.updateTag({property:'og:type', content:'artículo'});
        this.meta.updateTag({property:'og:description', content:'JC Developer Nicaragua te ofrece los mejores servicios en el desarrollo de tu sitio web, aplicacion y lo que necesites en el desarrollo de tu negocio a la par de la tecnologia'});
        this.meta.updateTag({property:'og:url', content:'http://www.jorgecastillodeveloper.pro/sobremi'});
        this.meta.updateTag({property:'og:image', content:'http://www.jorgecastillodeveloper.pro/assets/img/jorge.jpg'});
        this.meta.updateTag({property:'og:fb:admins', content:'https://www.facebook.com/jorgeacastillomoreno/'});
        // fin
        
        let params: InitParams = {
            appId: '1399875313450062',
            xfbml: true,
            version: 'v2.8'
              };
            
              fb.init(params);
	}

	ngOnInit()
	{
		console.log('AdminComponent cargado');
		//Consegur el listado de artista
        this.identity=this._userService.getIdentity();
        this.token=this._userService.getToken();

        console.log(this.identity);
        console.log(this.token);
       
        this.getperfil();
        this.getEducacion();

       

    }

    getperfil(){
        // this._route.params.forEach((params:Params)=>{
        //     let page=+params['page'];
        //     if (!page) {
        //         page=1;
        //     } else {
        //         this.next_page=page+1;
		// 		this.prev_page=page-1;

		// 		if (this.prev_page==0) {
		// 			this.prev_page=1;
        //         }
        //     }
        
        this._perfilService.getPerfil(this.token).subscribe(
            response=>{
            if (!response.perfils) {
                alert('No hay experiencias agregadas');
            } else {
                this.perfils=response.perfils;
            }
        },error=>{
            var errorMessage = <any>error;
            
            var body = JSON.parse(error._body);
            this.alertMessage = body.message;
            if (errorMessage !=null) { 
            this.alertMessage=body.message; 
            console.log(error);
            }
        });

    }


    getEducacion(){
        
        
        this._educacionService.getEducacion(this.token).subscribe(
            response=>{
            if (!response.educacions) {
                alert('No hay educacion agregadas');
            } else {
                this.educacions=response.educacions;
            }
        },error=>{
            var errorMessage = <any>error;
            
            var body = JSON.parse(error._body);
            this.alertMessage = body.message;
            if (errorMessage !=null) { 
            this.alertMessage=body.message; 
            console.log(error);
            }
        });

    }

    onDeletePerfil(id){
        this._perfilService.deleteOerfil(this.token,id).subscribe(response=>{
            if (!response.perfil) {
                alert('Error el el server');
            } else {
                this.getperfil();
            }
        },
        error=>{
                    var errorMessage = <any>error;

                var body = JSON.parse(error._body);
                this.alertMessage = body.message;
                if (errorMessage !=null) { 
                this.alertMessage=body.message; 
                console.log(error);
                }
            });
    }


    onDeleteEducacion(id){
        this._educacionService.deleteEducacion(this.token,id).subscribe(response=>{
            if (!response.educacion) {
                alert('Error el el server');
            } else {
                this.getEducacion();
            }
        },
        error=>{
                    var errorMessage = <any>error;

                var body = JSON.parse(error._body);
                this.alertMessage = body.message;
                if (errorMessage !=null) { 
                this.alertMessage=body.message; 
                console.log(error);
                }
            });
    }



}
