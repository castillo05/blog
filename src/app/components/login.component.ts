import { UserService } from '../service/user.service';
import { GLOBAL } from '../service/global';
import { User } from '../models/user';
import { Component,OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { FacebookService, LoginResponse, LoginOptions, UIResponse, UIParams, FBVideoComponent } from 'ngx-facebook';
import { Meta } from '@angular/platform-browser';


@Component({
  selector: 'login',
  templateUrl: '../views/login.html',
  styleUrls:['../css.component/login.component.css'],
  providers:[UserService]
})
export class LoginComponent implements OnInit {
 
  
  public identity;
  public token;
  //public posts:Post[];
  public user:User;
  //public postcat:Post;
  public url:String;
  public errorMessage;
  public alertMessage;
  public on;
 // public categoria:Categoria[];
 // public page=2;
 public alertRegister;

  constructor(
    private _userService:UserService,
    private _route:ActivatedRoute,
    private _router:Router,
    private fb: FacebookService,
    private meta:Meta

  )
  
  {
    this.user= new User('','','','','','','','');
    this.url=GLOBAL.url;
   
    

    this.meta.updateTag({ name: 'description', content: 'Hola soy jorge castillo y este es mi blog en donde publico tutoriales, articulos y muchas cosas mas sobre tecnologia y mas' });
        this.meta.updateTag({ name: 'author', content: 'Jorge Castillo Moreno' });
        this.meta.updateTag({ name: 'keywords', content: 'Jorge Castillo, Informatico, Programador Nicaragua, Tecnologia Nicaragua' });
        this.meta.updateTag({property:'og:title', content:'Jorge Castillo JC Developer Nicaragua Articulos Tutoriales sobre tecnologias'});
        this.meta.updateTag({property:'og:type', content:'artículo'});
        this.meta.updateTag({property:'og:description', content:'JC Developer Nicaragua te ofrece los mejores servicios en el desarrollo de tu sitio web, aplicacion y lo que necesites en el desarrollo de tu negocio a la par de la tecnologia'});
        this.meta.updateTag({property:'og:url', content:'http://www.jorgecastillodeveloper.pro'});
        this.meta.updateTag({property:'og:image', content:'http://www.jorgecastillodeveloper.pro/assets/img/jorge.jpg'});

        this.meta.updateTag({property:'og:fb:admins', content:'https://www.facebook.com/jorgeacastillomoreno/'});
  }
  ngOnInit(): void {
      this.identity=this._userService.getIdentity();
      this.token=this._userService.getToken();
     
    }

   
   
    onSubmit(){
        this._userService.singup(this.user).subscribe(response=>{
            let identity=response.user;
            this.identity=identity;
            //console.log(response.message);
            if(response.message){
              alert(response.message);
            }else if (!this.identity._id) {
                alert('El Usuario no esta correctamente logeado');
                console.log(this.identity);
            }else{
                //crear elemento del localStorage
                localStorage.setItem('identity',JSON.stringify(identity));
                //Conseguir el token para enviarlo a cada peticion
                this._userService.singup(this.user,'true').subscribe(response=>{
                    let token=response.token;
                    this.token=token;
                    if (this.token.length<=0) {
                        alert('El Token no se ha generado');
                     }else{
                         //se crea el elemento en el localStorage
                         localStorage.setItem('token',token);
                         this.user = new User('','','','','','','','');
                         this._router.navigate(['/']);

                     }
                },error=>{
                    var errorMessage=<any>error;
                    var body=JSON.stringify(error._body);
                    if (errorMessage!=null) {
                        this.errorMessage=body;
                        console.log(error);
                    }
                }
            );
                
            }
        },error=>{
            var errorMessage=<any>error;
            var body=JSON.stringify(error._body);
            if (errorMessage!=null) {
                this.errorMessage=body;
                console.log(error);
                alert(this.errorMessage);
            }
        }
    );
    }
  
   
   


    

   
 

}
