import { Educacion } from './../models/educacion';
import { Perfil } from './../models/perfil';
import { UserService } from './../service/user.service';
import { GLOBAL } from './../service/global';
import { User } from './../models/user';
import{Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { PerfilService } from '../service/perfil.service';
import { EducacionService } from '../service/educacion.service';


@Component({
	selector: 'admin',
    templateUrl:'../views/admin.html',
    styleUrls: ['../css.component/admin.component.css'],
    providers:[UserService,PerfilService,EducacionService]
})

export class PerfilAddComponent implements OnInit{
	public titulo:string;
    public user:User;
    public perfil:Perfil;
    public educacion:Educacion;
    public identity;
    public token;
    public url:String;
    public errorMessage;
    public alertMessage;

    public chek=false;


	constructor(
		private _route: ActivatedRoute,
        private _router: Router,
        private _userService:UserService,
        private _perfilService:PerfilService,
        private _educacionService:EducacionService
	

	){
		this.titulo= 'Guardar Perfil';
        this.user= new User('','','','','','','','');
        this.perfil=new Perfil('','','','','','');
        this.educacion=new Educacion('','','','','','');
        this.url=GLOBAL.url;
	}

	ngOnInit()
	{
		console.log('AdminComponent cargado');
		//Consegur el listado de artista
        this.identity=this._userService.getIdentity();
        this.token=this._userService.getToken();

        console.log(this.identity);
        console.log(this.token);
        if(this.identity.roles.roles!='administrador'){
            this._router.navigate(['/']);
        }

    }
    
    onSubmit(){
       
         this._perfilService.savePerfil(this.token,this.perfil).subscribe(response=>{
                if (!response.perfil) {
                    this.alertMessage=('Error en el servidor');
                } else {
                    this.alertMessage=('El Perfil se ha agregado correctamente');
                    this.perfil=response.perfil;
                    this._router.navigate(['/sobremi']);
                    console.log(this.perfil);

                }
            },
			error=>{
					var errorMessage = <any>error;

	                var body = JSON.parse(error._body);
	                this.alertMessage = body.message;
	                if (errorMessage !=null) { 
					this.alertMessage=body.message; 
					console.log(error);
					}
				});
    }


    

    }

    