import { GLOBAL } from './../service/global';
import { PostService } from './../service/post.service';
import { UserService } from './../service/user.service';
import{Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { User } from '../models/user';
import { Post } from '../models/post';



@Component({
	selector: 'home',
	templateUrl:'../views/categoria.html',
	//template:'  <div [innerHTML]="html | sanitizeHtml"></div>',
	styleUrls: ['../css.component/home.component.css'],
	providers:[UserService,PostService]
})

export class CategoriaComponent implements OnInit{
	public titulo:string;
    public posts:Post[];
    public publication:Post[];
	public identity;
    public token;
    public next_page;
	public prev_page;
	public alertmessage;
    public url;
    public page;
    public u:boolean;
    public img;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService:UserService,
		private _postService:PostService
	){
		this.titulo= 'jorgecastillodeveloper';
		this.next_page=1;
        this.prev_page=1;
        this.page=20;
        this.url=GLOBAL.url;
        this.u=true;
        
        
	}

	ngOnInit()
	{
		//console.log('HomeComponent cargado');
		//Consegur el listado de artista
		this.identity=this._userService.getIdentity();
		this.token=this._userService.getToken();
		
		//console.log(this.identity);
		//console.log(this.token);
        this.getPostCate();
       //this.getPostEnd();
		

	}    

	// getPostEnd(){
       

    //         this._postService.endPost().subscribe(response=>{
    //             if (!response.publication) {
    //                 this._router.navigate(['/']);
    //             } else {
    //                 this.publication=response.publication;
    //                 console.log(response.publication);
    //             }
    //         },error=>{
    //             var errorMessage = <any>error;
                
    //             var body = JSON.parse(error._body);
    //             this.alertmessage = body.message;
    //             if (errorMessage !=null) { 
    //             this.alertmessage=body.message; 
    //             console.log(error);
    //             }
    //         }
        
    //     );

    //     }

	getPostCate(){
        this._route.params.forEach((params:Params)=>{
            let id=params['id'];
            // let page=+params['page'];
            // if (!page) {
            //     page=10
            // } else {
            //     this.page=page+2;

            // }
        

            this._postService.getPostsCat(this.token,id).subscribe(response=>{
                if (!response.posts) {
                    this._router.navigate(['/']);
                }else if(response.total_items==0){
                    alert('No hay Post en esta categoria');
                    this._router.navigate(['/']);
                } else {
                    
                    this.posts=response.posts;
                    this.img=response.posts.image;
                    this.u=false;
                }
            },error=>{
                var errorMessage = <any>error;
                
                var body = JSON.parse(error._body);
                this.alertmessage = body.message;
                if (errorMessage !=null) { 
                this.alertmessage=body.message; 
                console.log(error);
                }
            }
        
        );

         });
    }
    
	

}