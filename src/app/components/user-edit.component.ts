import { User } from './../models/user';
import { PostService } from './../service/post.service';
import { UserService } from './../service/user.service';
import{Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Post } from '../models/post';
import { SelectControlValueAccessor } from '@angular/forms';


@Component({
	selector: 'home',
	templateUrl:'../views/user-edit.html',
	providers:[UserService,PostService]
})

export class UserEditComponent implements OnInit{
    public titulo:string;
    public posts:Post[];
    public user:User;
	public identity;
    public token;
    public next_page;
	public prev_page;
	public alertmessage;


	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
        private _userService:UserService,
        private _postService:PostService

	){
        this.titulo= 'Publicaciones';
        this.next_page=1;
        this.prev_page=1;
        this.user= new User('','','','','','','','');
	
	}

	ngOnInit()
	{
		console.log('PostListComponent cargado');
		//Consegur el listado de artista
		this.identity=this._userService.getIdentity();
		this.token=this._userService.getToken();

		console.log(this.identity);
		console.log(this.token);
        this.getUser();
    }

    getUser(){
        this._route.params.forEach((params:Params)=>{
            let id=params['id'];
            this._userService.getUserOne(this.token,id).subscribe(response=>{
                if (!response.userone) {
                    alert('Error...')
                } else {
                    if (this.identity.roles.roles!='administrador') {
                        alert('Usted no tiene privilegios de administrador');
                        this._router.navigate(['/']);
                    } 
                    this.user=response.userone;
                    console.log(response);
                }
            },
            error=>{
                        var errorMessage = <any>error;

                    var body = JSON.parse(error._body);
                    this.alertmessage = body.message;
                    if (errorMessage !=null) { 
                    this.alertmessage=body.message; 
                    console.log(error);
                    }
                });
        });
    }

   

    onSubmit(){
        this._route.params.forEach((params:Params)=>{
            let id= params['id'];

            this._userService.updateUser(this.token,this.user,id).subscribe(response=>{
            if (!response.user) {
                this.alertmessage=('Error en el servidor');

                } else {
                    alert('El Elemento se ha actualizado correctamente');
                    this.user=response.user;
                    this._router.navigate(['/userslist']);
                }
            },
            error=>{
                var errorMessage = <any>error;

                var body = JSON.parse(error._body);
                this.alertmessage = body.message;
                if (errorMessage !=null) { 
                this.alertmessage=body.message; 
                console.log(error);
                }
            }
        );
        });
    }
    
    }
    
