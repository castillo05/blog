export class Post{
    
    constructor(
        public _id:string,
        public title:string,
        public resume:string,
        public description:string,
        public date:string,
        public image:string,
        public video:string,
        public idvideo:string,
        public category:string,
        public user:string
    ){
        
    }



}