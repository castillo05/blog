import { Categoria } from './../models/categoria';
import{Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import  {GLOBAL} from './global';


@Injectable()

    export class CategoriaService{
        public url:String;
        public identity;
        public token;

        constructor(private _http:Http){
            this.url=GLOBAL.url;
        }

       getCategoria(token){
            let headers=new Headers({
                'Content-Type':'application/json',
                'Authorization':token
            });

            let options= new RequestOptions({headers:headers});
            return this._http.get(this.url+'getcategoria/',options).map(res=>res.json());
       }

       savecategoria(categoria:Categoria){
           let params=JSON.stringify(categoria);
        let headers=new Headers({
            'Content-Type':'application/json'
        });

        let options= new RequestOptions({headers:headers});
        return this._http.post(this.url+'agregar-categoria',params,{headers:headers}).map(res=>res.json());
       }

      
    }