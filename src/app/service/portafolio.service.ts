import { Portafolio } from './../models/portafolio';
import{Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import  {GLOBAL} from './global';


@Injectable()

    export class PortafolioService{
        public url:String;
        public identity;
        public token;

        constructor(private _http:Http){
            this.url=GLOBAL.url;
        }

       getPortafolios(token){
            let headers=new Headers({
                'Content-Type':'application/json',
                'Authorization':token
            });

            let options= new RequestOptions({headers:headers});
            return this._http.get(this.url+'getportafolios/',options)
            .map(res=>res.json());
       }

        savePortafolio(token,portafolio:Portafolio){
            let params = JSON.stringify(portafolio);
            let headers=new Headers({
                'Content-Type':'application/json',
                'Authorization':token
            });

            return this._http.post(this.url+'saveportafolio',params,{headers:headers})
		.map(res=>res.json());
        }

        getPortafolioone(token,id:String){
            let headers=new Headers({
                'Content-Type':'application/json',
                'Authorization':token
            });

            let options = new RequestOptions({headers:headers});
		    return this._http.get(this.url+'getportafolioone/'+id,options)
		    .map(res=>res.json());
        }

        updatePortafolio(token,id:String,portafolio:Portafolio){
            let params = JSON.stringify(portafolio);
            let headers=new Headers({
                'Content-Type':'application/json',
                'Authorization':token
            });

            let options= new RequestOptions({headers:headers});
            return this._http.put(this.url+'updateportafolio/'+id,params,{headers:headers})
            .map(res=>res.json());
       }

       deletePortafolio(token,id:String){
        let headers=new Headers({
			'Content-Type':'application/json',
			'Authorization':token
		});

		let options = new RequestOptions({headers:headers});
		return this._http.delete(this.url+'deleteportafolio/'+id,options)
		.map(res=>res.json());
       }
    }