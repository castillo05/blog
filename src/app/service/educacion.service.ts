import { Educacion } from './../models/educacion';
import{Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import  {GLOBAL} from './global';


@Injectable()

    export class EducacionService{
        public url:String;
        public identity;
        public token;

        constructor(private _http:Http){
            this.url=GLOBAL.url;
        }

       getEducacion(token){
            let headers=new Headers({
                'Content-Type':'application/json',
                'Authorization':token
            });

            let options= new RequestOptions({headers:headers});
            return this._http.get(this.url+'mostrareducacion/',options)
            .map(res=>res.json());
       }

       getEducacionOne(token,id:String){
        let headers=new Headers({
            'Content-Type':'application/json',
            'Authorization':token
        });

        let options = new RequestOptions({headers:headers});
		    return this._http.get(this.url+'geteducacion/'+id,options)
		    .map(res=>res.json());
       }

        saveEducacion(token,educacion:Educacion){
            let params = JSON.stringify(educacion);
            let headers=new Headers({
                'Content-Type':'application/json',
                'Authorization':token
            });

            return this._http.post(this.url+'saveeducacion',params,{headers:headers})
		.map(res=>res.json());
        }

        updateEducacion(token,id:String,educacion:Educacion){
            let params = JSON.stringify(educacion);
            let headers=new Headers({
                'Content-Type':'application/json',
                'Authorization':token
            });

            //let options= new RequestOptions({headers:headers});
            return this._http.put(this.url+'updateeducacion/'+id,params,{headers:headers})
            .map(res=>res.json());
        }

        deleteEducacion(token,id:String){
            let headers=new Headers({
                'Content-Type':'application/json',
                'Authorization':token
            });
    
            let options = new RequestOptions({headers:headers});
            return this._http.delete(this.url+'deleteeducacion/'+id,options)
            .map(res=>res.json());
           }

    }