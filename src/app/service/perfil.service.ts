import { Perfil } from './../models/perfil';
import{Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import  {GLOBAL} from './global';


@Injectable()

    export class PerfilService{
        public url:String;
        public identity;
        public token;

        constructor(private _http:Http){
            this.url=GLOBAL.url;
        }

       getPerfil(token){
            let headers=new Headers({
                'Content-Type':'application/json',
                'Authorization':token
            });

            let options= new RequestOptions({headers:headers});
            return this._http.get(this.url+'mostrarperfil/',options)
            .map(res=>res.json());
       }

        savePerfil(token,perfil:Perfil){
            let params = JSON.stringify(perfil);
            let headers=new Headers({
                'Content-Type':'application/json',
                'Authorization':token
            });

            return this._http.post(this.url+'saveperfil',params,{headers:headers})
		.map(res=>res.json());
        }

        getPerfilone(token,id:String){
            let headers=new Headers({
                'Content-Type':'application/json',
                'Authorization':token
            });

            let options = new RequestOptions({headers:headers});
		    return this._http.get(this.url+'getperfil/'+id,options)
		    .map(res=>res.json());
        }

        updatePerfil(token,id:String,perfil:Perfil){
            let params = JSON.stringify(perfil);
            let headers=new Headers({
                'Content-Type':'application/json',
                'Authorization':token
            });

            //let options= new RequestOptions({headers:headers});
            return this._http.put(this.url+'updateperfil/'+id,params,{headers:headers})
            .map(res=>res.json());
       }

       deleteOerfil(token,id:String){
        let headers=new Headers({
			'Content-Type':'application/json',
			'Authorization':token
		});

		let options = new RequestOptions({headers:headers});
		return this._http.delete(this.url+'deleteperfil/'+id,options)
		.map(res=>res.json());
       }
    }