import{Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import  {GLOBAL} from './global';
import { Post } from './../models/post';




    @Injectable()

    export class PostService{
        public url:String;
        public identity;
        public token;

        constructor(private _http:Http){
            this.url=GLOBAL.url;
        }

       getPosts(token,page){
            let headers=new Headers({
                'Content-Type':'application/json',
                'Authorization':token
            });

            let options= new RequestOptions({headers:headers});
            return this._http.get(this.url+'posts/'+page,options).map(res=>res.json());
       }

       getPostsCat(token,id:String){
        let headers=new Headers({
            'Content-Type':'application/json',
            'Authorization':token
        });

        let options= new RequestOptions({headers:headers});
        return this._http.get(this.url+'filterpost/'+id+'/',options).map(res=>res.json());
   }

//    Metodo para contar numero de post segun categorias

        getcountPost(){
            let headers=new Headers({
                'Content-Type':'application/json'
            });

            let options= new RequestOptions({headers:headers});
            return this._http.get(this.url+'countpost/',options).map(res=>res.json());
        }

// Fin


       endPost(){
        let headers=new Headers({
            'Content-Type':'application/json'
        });

        let options= new RequestOptions({headers:headers});
        return this._http.get(this.url+'endpost',options).map(res=>res.json());
       }

       getPostsOne(slug:String){
        let headers=new Headers({
            'Content-Type':'application/json'
        });

        let options= new RequestOptions({headers:headers});
        return this._http.get(this.url+'getpostone/'+slug,options).map(res=>res.json());
         }

        //  Metodo para buscar PPost
        getsearchpost(search:String){
            let headers= new Headers({
                'Content-Type':'application/json'
            });

            let option=new RequestOptions({headers:headers});

            return this._http.get(this.url+'searchpost/'+search,option).map(res=>res.json());
        }   
        // Fin
        getPostsOneEdit(token,slug:String){
            let headers=new Headers({
                'Content-Type':'application/json'
            });

            let options= new RequestOptions({headers:headers});
            return this._http.get(this.url+'getpostone/'+slug,options).map(res=>res.json());
        }

        savePost(token,post:Post){
            let params = JSON.stringify(post);
            let headers = new Headers({
                'Content-Type':'application/json',
                'Authorization':token
            });

            let options= new RequestOptions({headers:headers});
             return this._http.post(this.url+'agregar-post',params,{headers:headers}).map(res=>res.json());

        }

        updatePost(token,id:String,post:Post){
            let params = JSON.stringify(post);
            let headers=new Headers({
                'Content-Type':'application/json',
                'Authorization':token
            });

            let options= new RequestOptions({headers:headers});
            return this._http.put(this.url+'updatepost/'+id,params,{headers:headers})
            .map(res=>res.json());
       }

       deletePost(token,id:String){
        
        let headers=new Headers({
            'Content-Type':'application/json',
            'Authorization':token
        });

        let options= new RequestOptions({headers:headers});
        return this._http.delete(this.url+'deletepost/'+id,{headers:headers})
        .map(res=>res.json());
   }

    }