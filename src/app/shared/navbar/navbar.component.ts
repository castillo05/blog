
import { Post } from '../../models/post';
import { UserService } from '../../service/user.service';
import { GLOBAL } from '../../service/global';
import { User } from '../../models/user';
import { Component,OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { FacebookService, LoginResponse, LoginOptions, UIResponse, UIParams, FBVideoComponent,InitParams } from 'ngx-facebook';
import { Categoria } from '../../models/categoria';
import { CategoriaService } from '../../service/categoria.service';
import { PostService } from '../../service/post.service';
import { Meta } from '@angular/platform-browser';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public identity;
  public token;
  public posts:Post[];
  public user:User;
  public postcat:Post[];
  public cat:Categoria;
  public url:String;
  public errorMessage;
  public alertMessage;
  public on;
  public categoria:Categoria;
  public page=2;
  public closeResult: string;
  public search:String;

  private countp;
  public cate;
  private nombrecat;
  private idcat;
  private contador=0;
  constructor(
    private _userService:UserService,
    private _route:ActivatedRoute,
    private _router:Router,
    private fb: FacebookService,
    private _categoriaService:CategoriaService,
    private _postService:PostService,
    private meta:Meta,
    private modalService: NgbModal

  ) {
    this.user= new User('','','','','','','','');
    this.url=GLOBAL.url;

    let params: InitParams = {
      appId: '1399875313450062',
      xfbml: true,
      version: 'v2.8'
        };

        fb.init(params);
  }

  ngOnInit() {
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
    this.getCategoria();
  }


  getCategoria(){




    this._categoriaService.getCategoria(this.token).subscribe(response=>{
        if (!response.categoria) {
            console.log('No Hay categoria');
        } else {
            this.cate=response.categoria;

           this._postService.getcountPost().subscribe(resp=>{
               this.countp=resp.postcat;
                for (let i = 0; i < this.countp.length; i++) {
                    const element = this.countp[i];
                        for (let j = 0; j < this.cate.length; j++) {
                            const element2=this.cate[j];
                           if (element.category==element2._id) {
                               this.contador=this.contador+1;
                               console.log(this.contador);
                           }

                       }
                }
           });

        }


},
error=>{
var errorMessage = <any>error;

var body = JSON.parse(error._body);
this.alertMessage = body.message;
if (errorMessage !=null) {
this.alertMessage=body.message;
console.log(error);
}
});

}

// Metodo para contar los post por categoria



SearchPost(search){
//this._route.params.forEach((params:Params)=>{
if(search==''){
return this._router.navigate(['/']);
}
this.search=search;

// let page=+params['page'];
// if (!page) {
//     page=2
// } else {
//     this.page=page+2;

// }


this._postService.getsearchpost(this.search).subscribe(response=>{
if (!response.searchPost) {
    this._router.navigate(['/']);
}else {

    this.posts=response.searchPost;
    this._router.navigate(['/SearchPost/'+this.search]);
}
},error=>{
var errorMessage = <any>error;

var body = JSON.parse(error._body);
this.alertMessage = body.message;
if (errorMessage !=null) {
this.alertMessage=body.message;
console.log(error);
}
}

);

// });


}





onSubmit(){
this._userService.singup(this.user).subscribe(response=>{
let identity=response.user;
this.identity=identity;
//console.log(response.message);
if(response.message){
alert(response.message);
}else if (!this.identity._id) {
alert('El Usuario no esta correctamente logeado');
console.log(this.identity);
}else{
//crear elemento del localStorage
localStorage.setItem('identity',JSON.stringify(identity));
//Conseguir el token para enviarlo a cada peticion
this._userService.singup(this.user,'true').subscribe(response=>{
  let token=response.token;
  this.token=token;
  if (this.token.length<=0) {
      alert('El Token no se ha generado');
   }else{
       //se crea el elemento en el localStorage
       localStorage.setItem('token',token);
       this.user = new User('','','','','','','','');
   }
//    this._router.navigate(['/']);
},error=>{
  var errorMessage=<any>error;
  var body=JSON.stringify(error._body);
  if (errorMessage!=null) {
      this.errorMessage=body;
      console.log(error);
  }
}
);

}
},error=>{
var errorMessage=<any>error;
var body=JSON.stringify(error._body);
if (errorMessage!=null) {
this.errorMessage=body;
console.log(error);
alert(this.errorMessage);
}
}
);
}

logout(){
localStorage.removeItem('identity');
localStorage.removeItem('token');
localStorage.clear();
this.identity=null;
this.token=null;
this._router.navigate(['/']);
}

}
