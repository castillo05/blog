import { PostFilterComponent } from './components/post-filter.component';
import { UserEditComponent } from './components/user-edit.component';
import { PortafolioAddComponent } from './components/portafolio-add.component';
import { PerfileditComponent } from './components/perfil-edit.component';
import { PostDetailsComponent } from './components/post-details.component';
import { Post } from './models/post';
import { SobremiComponent } from './components/sobremi.component';
import { AdminComponent } from './components/admin.component';
import { HomeComponent } from './components/home.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import{routing,appRoutingProviders} from './app.routing';
import { AppComponent } from './app.component';
import { PostListComponent } from './components/post-list.component';
import { PerfilAddComponent } from './components/perfil-add.component';
import { EducacionAddComponent } from './components/educacion-add.component';
import { EducacioneditComponent } from './components/educacion-edit.component';
import { PortafolioComponent } from './components/portafolio.component';
import { PortafolioeditComponent } from './components/portafolio-edit.component';
import { ServiceComponent } from '../app/components/service.component';
import { ContactoComponent } from '../app/components/contacto.component';
import { PostAddComponent } from './components/post-add.component';
import { CKEditorModule } from 'ng2-ckeditor';
import { PosteditComponent } from './components/post-edit.component';
import { VgCoreModule } from 'videogular2/core';
import { FacebookModule } from 'ngx-facebook';
import { CategoriaComponent } from './components/categoria.component';
import {AdsenseModule} from 'ng2-adsense';
import { SearchComponent } from './components/search.component';
import { RegisterComponent } from './components/register.component';
import { UserListComponent } from './components/user-list.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SimpleNotificationsModule } from 'angular2-notifications';
import {JasperoAlertsModule} from '@jaspero/ng2-alerts';
import {YoutubePlayerModule} from 'ngx-youtube-player';

// componente de login

import{LoginComponent} from './components/login.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FacecomponentComponent } from './shared/facecomponent/facecomponent.component';
import { CoverComponent } from './cover/cover.component';
import { FooterComponent } from './shared/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AdminComponent,
    SobremiComponent,
    PostListComponent,
    PostDetailsComponent,
    PerfileditComponent,
    PerfilAddComponent,
    EducacionAddComponent,
    EducacioneditComponent,
    PortafolioComponent,
    PortafolioAddComponent,
    PortafolioeditComponent,
    ServiceComponent,
    ContactoComponent,
    PostAddComponent,
    PosteditComponent,
    CategoriaComponent,
    SearchComponent,
    RegisterComponent,
    UserListComponent,
    UserEditComponent,
    PostFilterComponent,
    LoginComponent,
    NavbarComponent,
    FacecomponentComponent,
    CoverComponent,
    FooterComponent
   
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    routing,
    CKEditorModule,
    VgCoreModule,
    FacebookModule.forRoot(),
    NgbModule.forRoot(),
    AdsenseModule.forRoot ({
      adClient: 'ca-pub-9497107609585223', // reemplazar con su cliente de adsense de google
      adSlot: 3266185512 // reemplace con su ranura de google adsense
  }),
  SimpleNotificationsModule.forRoot(),
  JasperoAlertsModule,
  YoutubePlayerModule
  ],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
